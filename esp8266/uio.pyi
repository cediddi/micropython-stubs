from typing import BinaryIO, TextIO, IO

FileIO = IO
TextIOWrapper = IO
StringIO = TextIO
BytesIO = BinaryIO


def open(name: str, mode: str = ...) -> TextIOWrapper: ...
